const path = require('path')

function resolve (dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  chainWebpack: (config)=>{
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets',resolve('src/assets'))
      .set('components',resolve('src/components'))
      .set('views',resolve('src/views'))
      .set('mixins',resolve('src/mixins'))
    config.plugin('html')
      .tap(args => {
        args[0].chunksSortMode = 'none'
        return args
      })
    // config.plugins.delete('prefetch')
    // config.plugin('preload')
  },

  devServer: {
    proxy: {
      'api/v1': {
        target: 'http://localhost:8111'
      }
    }
  },

  configureWebpack: {
    plugins: [
    ],
    entry: {
      app: './src/main.js'
    },
    output: {
      filename: '[id].[hash].js',
      chunkFilename: '[id].[hash].js'
    },
    optimization: {
      providedExports: true,
      usedExports: true,
      sideEffects: true,
      concatenateModules: true,
      noEmitOnErrors: true,
      splitChunks: {
        chunks: 'async',
        cacheGroups: {
          vendors: {
            chunks: 'async',
            minChunks: 2,
            maxInitialRequests: 5,
            minSize: 0,
            priority: 15,
            name: 'vendor'
          }
        }
      },
      runtimeChunk: true
    }
  },

  pwa: {
    name: 'f2e-test',
    msTileColor: '#B8E986',
    themeColor: '#4283E4'
  }
}
