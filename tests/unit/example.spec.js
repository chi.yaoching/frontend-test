import { shallowMount } from '@vue/test-utils'
import Card from '@/components/Card.vue'

describe('Card.vue', () => {
  const info = {
    captions: ['cht'],
    collectCount: 7689,
    duration: 368,
    id: 0,
    level: 3,
    publish: 1422836632,
    thumbnail: "https://cdn.voicetube.com/assets/thumbnails/wWV0NCPD050.jpg",
    title: "Quisquam voluptatem quaerat sed est.",
    views: 32409534,
  }
  let wrapper = shallowMount(Card)
  let vm = wrapper.vm

  it('檢查語言轉換是否正確', () => {
    expect(vm.$__langObj(info.captions[0]).text).toMatch('中文')
  })

  it('檢查課程級別是否正確', () => {
    expect(vm.$__levelObj(info.level).text).toMatch('中高級')
  })
})
