FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

ARG buildtime_version=''
ENV version=$buildtime_version

EXPOSE 8111
CMD [ "node", "server/index.js" ]

