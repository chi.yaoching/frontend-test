import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {
    async fetchVideos () {
       return await axios.get('https://us-central1-lithe-window-713.cloudfunctions.net/fronted-demo')
    }
  }
})
