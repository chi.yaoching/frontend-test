/**
 * Created by Seven YaoChing-Chi on 2019/03/08.
 */
const express = require('express')
const app = express()
const path = require('path')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const compression = require('compression')
// 略過 ＳＳＬ憑證問題
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())
app.use(compression()) // gzip mode.

app.set('port', 8111)
app.use('/', express.static(resolve('/dist')))

app.get('*', (req, res) => {
  res.sendFile(path.join(resolve('/dist/index.html')))
})

app.listen(app.get('port'), () => {
  console.log('Visit http://localhost:' + app.get('port'))
})
